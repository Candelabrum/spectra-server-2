'use strict';

exports.commands = {
	customicon: 'icon',
	icon: function (target, room, user) {
		if (!user.can('rangeban')) return false;
		target = target.split(',');
		for (let u in target) target[u] = target[u].trim();
		if (!target[1]) return this.parse('/help icon');
		if (toId(target[0]).length > 19) return this.errorReply("Usernames are not this long...");
		if (target[1] === 'delete') {
			if (!Spectra.icons[toId(target[0])]) return this.errorReply('/icon - ' + target[0] + ' does not have an icon.');
			delete Spectra.icons[toId(target[0])];
			Spectra.writeIcons();
			this.sendReply("You removed " + target[0] + "'s icon.");
			Rooms('upperstaff').add(user.name + " removed " + target[0] + "'s icon.").update();
			if (Users(target[0]) && Users(target[0]).connected) Users(target[0]).popup(user.name + " removed your icon.");
			return;
		} else {
			this.sendReply("|raw|You have given <b><font color=" + Spectra.hashColor(Chat.escapeHTML(target[0])) + ">" + Chat.escapeHTML(target[0]) + "</font></b> an icon.");
			Rooms('upperstaff').add('|raw|<b><font color="' + Spectra.hashColor(Chat.escapeHTML(target[0])) + '">' + Chat.escapeHTML(target[0]) + '</font> has received an icon from ' + Chat.escapeHTML(user.name) + '.</b>').update();
			Spectra.icons[toId(target[0])] = target[1];
			Spectra.writeIcons();
		}
	},
	iconhelp: [
		"Commands Include:",
		"/icon [user], [image url] - Gives [user] an icon of [image url]",
		"/icon [user], delete - Deletes a user's icon",
	],
};
