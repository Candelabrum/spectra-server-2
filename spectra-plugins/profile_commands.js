"use strict";

exports.commands = {
	profile: {
		'': 'view',
		view: function(target, room, user) {
			if (!target) target = user.name;
			const userid = toId(target);
			if (userid.length > 19) return this.sendReply("Usernames may not be more than 19 characters long.");
			if (userid.length === 0) return this.sendReply("err");
			Spectra.UserClass.display(user, userid);
		},

		mini: function(target, room, user) {
			if (!target) target = user.name;
			if (!this.runBroadcast()) return;
			const userid = toId(target);
			if (userid.length > 19) return this.sendReply("Usernames may not be more than 19 characters long.");
			if (userid.length === 0) return this.sendReply("err");
			Spectra.UserClass.displayMini(room, this, user, userid);
		},

		edit: function(target, room, user) {
			if (!target) return this.parse("/profile help");
			target = target.split(",");
			const key = toId(target.shift()).trim();
			let bool = false, err = ""; //Pass

			switch(key) {
			case "about":
				if (!target[0]) return this.parse("/profile help");
				if (user.level >= 3 || user.isStaff) {
					target = Chat.escapeHTML(target.join(","));
					if (target.length > 500) {
						err = "About me cannot be longer than 500 characters.";
						break;
					}
					user.editProfile("about", target);
					bool = `Your Profile About Me has been set.`;
				}
				break;
			case "background":
				if (!target[0]) return this.parse("/profile help");
				if (user.level >= 7 || user.isStaff) {
					target = Chat.escapeHTML(toId(target.join(","))).trim();
					if (!Spectra.verifyHex(target)) {
						err = "Invalid Hexidecimal Input";
						break;
					}
					user.editProfile("background", Spectra.hexToRgb(target).join("|"));
					bool = `Your Profile background has been set to #${toId(target)}.`;
				}
				break;
			case "team":
				if (!target[0]) return this.parse("/profile help");
				if (user.level >= 13 || user.isStaff) {
					if (target.length > 6) {
						err = "Too many pokemon selected";
						break;
					}

					bool = []; //Shut up, this isnt typecasted, i can do this
					for (let i = 1; i <= target.length; i++) {
						if (toId(target[i].trim()) === "clear") {
							bool = "clear";
							break;
						} else {
							if (Spectra.validatePoke(toId(target[i].trim()))) {
								bool.push(toId(target[i].trim()));
							}
						}
					}

					if (bool === "clear") {
						for (let i = 1; i < 7; i++) {
							user.editProfile(`p${i}`, null);
						}
						bool = "Your Profile team has successfully been cleared.";
					} else if (typeof bool === "object" && bool.length === 0) {
						bool = false;
						err = "No valid Pokemon names entered";
						break;
					} else {
						target = [];
						for (let i = 1; i <= bool.length; i++) {
							user.editProfile(`p${i}`, bool[i]);
							target.push(Spectra.validatePoke(bool[i]));
						}
						bool = `Your Profile team has been set to: ${target.join(", ")}`;
					}
				}
				break;
			case "primarycolor":
				if (!target[0]) return this.parse("/profile help");
				if (user.level >= 18 || user.isStaff) {
					target = Chat.escapeHTML(toId(target.join(","))).trim();
					if (!Spectra.verifyHex(target)) {
						err = "Invalid Hexidecimal Input";
						break;
					}
					user.editProfile("color", Spectra.hexToRgb(target).join("|"));
					bool = `Your Profile Primary Color has been set to #${toId(target)}.`;
				}
				break;
			case "secondarycolor":
				if (!target[0]) return this.parse("/profile help");
				if (user.level >= 18 || user.isStaff) {
					target = Chat.escapeHTML(toId(target.join(","))).trim();
					if (!Spectra.verifyHex(target)) {
						err = "Invalid Hexidecimal Input";
						break;
					}
					user.editProfile("color2", Spectra.hexToRgb(target).join("|"));
					bool = `Your Profile Secondary Color has been set to #${toId(target)}.`;
				}
				break;
			default: return this.parse("/profile help");
			}

			if (!bool) {
				return this.sendReply(err ? err : "/profile edit - Access Denied.");
			} else {
				return this.sendReply(bool);
			}
		},

		help: function() {
			if (!this.canBroadcast) return false;
			return this.sendReplyBox(
				"/profile view [user] - Displays the profile of the user. Not recommended for mobile displays. If you wish to view your profile, simply use /profile.<br />" +
				"/profile mini [user] - Displays the mini profile of a user.<br/>" +
				"/profile edit [element], [new value] - Allows you to Customize your profile. The following things can be changed:<br/>" +
				"[unlocked at level 3] --- about - Lets you change the text in the second panel of your profile. Character limit of 500 characters.<br/>" +
				"[unlocked at level 7] --- background - Lets you change the background of your profile. You must specify a valid hex color (example: #000000)<br/>" +
				"[unlocked at Level 13] --- team - Lets you change the Pokemon in the top right corner of your profile. May choose between 1 and 6. Must be valid Pokemon names.<br/>" +
				"Example of the correct command when setting multiple Pokemon: `/profile edit team, bulbasaur, charmander, squirtle`.<br/>" +
				"To remove the Team from your profile, use: `/profile edit team, clear`.<br/>" +
				"[unlocked at level 18] --- primarycolor - Lets you change the primary color of your profile. You must specify a valid hex color (example: #000000)<br/>" +
				"[unlocked at level 18] --- secondarycolor - Lets you change the secondary color of your profile. You must specify a valid hex color (example: #000000)<br/>" +
				"Profile overhaul by Lights."
			);
		}
	}
};