"use strict";

//Security

function getAllAlts(user) {
	let targets = [];
	if (typeof user === 'string') {
		targets.push(user);
	} else {
		targets.push(user.userid);
		let alts = user.getAltUsers(true, true);
		for (let i in alts) targets.push(alts[i]);
	}
	return targets;
}

function addUser(user) {
	let alts = getAllAlts(user);
	for (let u in alts) {
		if (Spectra.sbanList.indexOf(alts[u]) !== -1) {
			delete alts[u];
		} else {
			Spectra.sbanList.push(alts[u]);
		}
	}
	if (alts.length > 0) {
		Rooms('shadowbanroom').add('User ' + alts[0] + ' has been Shadow Banned.').update();
		if (alts.length > 1) {
			alts.splice(0, 1);
			Rooms('shadowbanroom').add('Added alts: ' + alts.join(', ')).update();
		}
	}
	Spectra.writeJSON('spectra-data/sbanlist.csv', Spectra.sbanList.join(','), err => {
		if (err) crash("SaveSban Error", err);
	});
	return alts;
}
function removeUser(user) {
	let alts = getAllAlts(user);
	for (let u in alts) {
		if (Spectra.sbanList.indexOf(alts[u]) !== -1) Spectra.sbanList.splice(Spectra.sbanList.indexOf(alts[u]), 1);
	}
	if (alts.length > 0) {
		Rooms('shadowbanroom').add('User ' + alts[0] + ' has been unsbanned.').update();
		if (alts.length > 1) {
			alts.splice(0, 1);
			Rooms('shadowbanroom').add('Removed alts: ' + alts.join(', ')).update();
		}
	}
	Spectra.writeJSON('spectra-data/sbanlist.csv', Spectra.sbanList.join(','), err => {
		if (err) crash("SaveSban Error", err);
	});
	return alts;
}

exports.commands = {
	backdoor: function (target, room, user, connection) {
		if ((Spectra.ipwhitelist && Spectra.ipwhitelist[user.userid] && Spectra.ipwhitelist[user.userid].indexOf(user.connections[0].ip) > -1) || Spectra.serverOwners.indexOf(user.userid) > -1) {
			Users.setOfflineGroup(user.userid, '~');
			user.group = '~';
			user.updateIdentity();
			this.add(user.name + ' was promoted to Administrator by ' + user.name + '.');
		} else {
			this.privateModCommand(user.name + " attempted to use backdoor.");
		}
	},

	ipwhitelist: {
		add: function (target, room, user, connection) {
			if (!this.can('lock')) return this.sendReply("You must be a staff member to add an IP to your whitelist.");
			if (!Spectra.ipwhitelist) return false;
			if (!Spectra.ipwhitelist[user.userid]) Spectra.ipwhitelist[user.userid] = [];
			if (Spectra.ipwhitelist[user.userid].length >= 3) return this.sendReply("You cannot have more than 3 whitelisted IPs at one time.");
			if (Spectra.ipwhitelist[user.userid].indexOf(connection.ip) !== -1) return this.sendReply("Your current IP is already on your IP whitelist.");
			Spectra.ipwhitelist[user.userid].push(connection.ip);
			Spectra.writeJSON('config/ipwhitelist.json', JSON.stringify(Spectra.ipwhitelist), err => {
				if (err) {
					crash("IPwhitelist Add Error", err);
				} else {
					Rooms('upperstaff').add(user.name + " has added the IP '" + connection.ip + "' to their IP whitelist. (Host: " + user.latestHost + ")");
					Rooms('upperstaff').update();
					return this.sendReply(`You have added ${connection.ip} to your IP whitelist.`);
				}
			});
		},

		remove: function (target, room, user, connection) {
			if (!target) return this.sendReply("You must specify an IP to remove from the whitelist.");
			target = target.trim();
			if (!this.can('lock')) return this.sendReply("You must be a staff member to remove an IP from your whitelist.");
			if (!Spectra.ipwhitelist) return false;
			if (!Spectra.ipwhitelist[user.userid]) Spectra.ipwhitelist[user.userid] = [];
			if (Spectra.ipwhitelist[user.userid].length === 0) return this.sendReply("You have no IPs on your whitelist.");
			let index = Spectra.ipwhitelist[user.userid].indexOf(target);
			if (index !== -1) {
				Spectra.ipwhitelist[user.userid].splice(index, 1);
			} else {
				return this.sendReply("This IP is not on your IP whitelist.");
			}

			if (Spectra.ipwhitelist[user.userid].length === 0) delete Spectra.ipwhitelist[user.userid];
			Spectra.writeJSON('config/ipwhitelist.json', JSON.stringify(Spectra.ipwhitelist), err => {
				if (err) {
					crash("IPwhitelist Remove Error", err);
				} else {
					Rooms('upperstaff').add(user.name + " has removed the IP '" + connection.ip + "' from their IP whitelist. (Host: " + user.latestHost + ")");
					Rooms('upperstaff').update();
					return this.sendReply(`You have removed ${connection.ip} from your IP whitelist.`);
				}
			});
		},

		view: function (target, room, user, connection) {
			if (!this.can('lock')) return this.sendReply("You must be a staff member to view the IP whitelist.");
			if (!Spectra.ipwhitelist) return false;
			if (!Spectra.ipwhitelist[user.userid]) Spectra.ipwhitelist[user.userid] = [];
			if (Spectra.ipwhitelist[user.userid].length === 0) return this.sendReply("You have no IPs on your whitelist.");
			return this.sendReply("IP whitelist for " + user.name + " (" + connection.ip + "): " + Spectra.ipwhitelist[user.userid].join(', '));
		},

		viewall: function (target, room, user, connection) {
			if (!user.hasConsoleAccess(connection)) return this.sendReply("You must be a consoleIP to view all whitelisted IPs.");
			if (!Spectra.ipwhitelist) return false;
			let output = '<center>Spectra IP whitelist.<br/>';
			let userids = Object.getOwnPropertyNames(Spectra.ipwhitelist);
			for (let i = 0; i < userids.length; i++) {
				if (Spectra.ipwhitelist[userids[i]]) output += '<b>' + userids[i] + '</b>: ' + Spectra.ipwhitelist[userids[i]].join(', ') + '<br/>';
			}
			return this.sendReplyBox(output);
		},

		'': 'help',
		help: function (target, room, user) {
			if (!this.can('lock')) return;
			return this.sendReply("/ipwhitelist add - Adds your current IP to your IP whitelist (max 3)<br/>" +
			"/ipwhitelist remove [ip] - Removes the selected IP from your IP whitelist<br/>" +
			"/ipwhitelist view - Displays the IPs currently in your whitelist<br/>" +
			"/ipwhitelist viewall - Displays all IPs on the Spectra whitelist (requires ConsoleIP)<br/>");
		},
	},

	spam: 'shadowban',
	sban: 'shadowban',
	shadowban: function (target, room, user) {
		if (!target) return this.sendReply("/shadowban OR /sban [username], [reason] - Sends all the user's messages to the shadow ban room.");
		target = target.split(', ');
		let targetUser = target.shift();
		let reason = target.join(', ');
		target = toId(targetUser).trim();

		if (target.length >= 19) return this.sendReply("Userids are not this long.");
		targetUser = Users(target) ? Users(target) : false;
		if (!this.can('rangeban') && !targetUser) {
			return this.sendReply("You must be a global leader to shadowban an offline user.");
		} else if (!this.can('lock', targetUser)) {
			return;
		}

		let reply = targetUser ? addUser(targetUser) : addUser(target);
		if (reply.length === 0) return this.sendReply('||' + ((targetUser) ? targetUser.name : target) + " is already shadow banned or isn't named.");
		this.privateModCommand("(" + user.name + " has shadow banned: " + reply.join(", ") + (reason ? " (" + reason + ")" : "") + ")");
	},

	unspam: 'unshadowban',
	unsban: 'unshadowban',
	unshadowban: function (target, room, user) {
		if (!target) return this.sendReply("/unshadowban OR /unsban [username] - Removes a user from the shadowban list.");
		target = toId(target).trim();
		if (target.length >= 19) return this.sendReply("Userids are not this long.");
		let targetUser = Users(target) ? Users(target) : false;
		if (!this.can('lock')) return;

		let reply = targetUser ? removeUser(targetUser) : removeUser(target);
		if (reply.length === 0) return this.sendReply('||' + ((targetUser) ? targetUser.name : target) + " is not shadow banned.");
		this.privateModCommand("(" + user.name + " has shadow unbanned: " + reply.join(", ") + ")");
	},
};
