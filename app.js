'use strict';

const FS = require('./fs');
global.Monitor = new (require('./lights/monitor'))(process.pid).init();
Monitor.on("ready", m => {
	require("./lights/crashlogger.js")(m);

	Object.assign(Monitor, require("./monitor"));

	global.Config = require('./config/config');

	if (Config.watchconfig) {
		let configPath = require.resolve('./config/config');
		FS(configPath).onModify(() => {
			try {
				delete require.cache[configPath];
				global.Config = require('./config/config');
				if (global.Users) Users.cacheGroupData();
				Monitor.notice('Reloaded config/config.js');
			} catch (e) {
				Monitor.adminlog(`Error reloading config/config.js: ${e.stack}`);
			}
		});
	}

	process.emit("spectraInit");
});

process.on("spectraInit", () => {
	new Promise((resolve, reject) => {
		try {
			global.Spectra = new (require('./spectra'))(new Map([
				['Dex', './sim/dex'],
				['LoginServer', './loginserver'],
				['Ladders', './ladders'],
				['Users', './users'],
				['Punishments', './punishments'],
				['Chat', './chat'],
				['Rooms', './rooms'],
				['Verifier', './verifier'],
				['Tournaments', './tournaments'],
				['Dnsbl', './dnsbl'],
			], [
				'Verifier.PM.spawn();',
				'Dnsbl.loadDatacenters();',
			]));
		} catch (e) {
			reject(e);
		}

		Spectra.on("ready", () => resolve());

		Spectra.init();
	}).then(() => {
		global.Sockets = require('./sockets');

		exports.listen = function (port, bindAddress, workerCount) {
			Sockets.listen(port, bindAddress, workerCount);
		};

		if (require.main === module) {
			// Launch the server directly when app.js is the main module. Otherwise,
			// in the case of app.js being imported as a module (e.g. unit tests),
			// postpone launching until app.listen() is called.
			let port;
			if (process.argv[2]) port = parseInt(process.argv[2]);
			Sockets.listen(port);
		}

		global.TeamValidatorAsync = require('./team-validator-async');
		TeamValidatorAsync.PM.spawn();

		require('./repl').start('app', cmd => eval(cmd));
	}).catch(e => {
		crash("Unable to load Spectra", e, true);
	});
});
