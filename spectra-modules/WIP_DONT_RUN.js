"use strict";

const msgpack = require("msgpack-js");
const MAX_SIZE = 1000000;

function HexToBinary(buffer) {
	let bytes = [];
	for (let i = 0; i < buffer.length - 1; i+=2) {
		bytes.push(hex.substr(i,2), 16);
	}
	return bytes.join("");
}

class Cell {
	constructor(id, data) {
		this.id = id;
		this.data = data;
		this.index = null; //object containing entries this cell holds and their size in bytes
		this.dataSize = 0; //In Bytes
	}

	updateIndex() {
		//Index the data in this cell and return it to the wrapper
		if (!this.index) this.index = {};
		let entry;
		for (const i in this.data) {
			this.index[i] = {
				id: i,
				size: HexToBinary(msgpack.encode(this.data[i])).length,
				cell: this.id
			};
		}
		this.updateSize();
		return this.index;
	}

	updateSize() {
		this.dataSize = HexToBinary(msgpack.encode(this.data)).length;
		if (this.dataSize >= MAX_SIZE) {
			this.emit("divide", this);
		}
	}

	preSplit() {
		//We're going to need to prepare a split, so we need to figure out
		//what we're getting rid of
	}
}

class DeadCell {
	constructor(id, bin, error) {
		this.id = id;
		this.bin = bin;
		this.error = error;
	}
}

const PDB_Wrapper = module.exports = class extends require("events") {
	constructor() {
		super();

		this.cells = new Map();
		this.deadCells = new Map();
		this.index = {};
	}

	init() {

		//Start building cells
		Spectra.loadMsgpackDB((arr) => {
			this.decode(arr[0],arr[1]);
		});
	}

	decode(id, bin) {

		//Decode msgpack binary
		let data, err;

		try {
			data = msgpack.decode(bin);
		} catch (e) {
			console.log("error decoding " + id);
			console.log(e);
			data = null;
			err = e;
		}

		if (data) {
			//Valid data - creating Cell
			const c = new Cell(id, data);

			//Listen for when a cell has divided
			c.on("divide", (cell) => {
				this.divide(cell);
			});

			//Add this to the Cell Set
			this.cells.set(id, c);
			Object.assign(this.index, c.updateIndex());
		} else {
			//Couldnt decode data, creating dead cell
			const d = new DeadCell(id, bin, err);
			this.deadCells.set(id, d);

			//we'll try to salvage this
		}
	}

	divide(cell) {
		//A cell is being forced to divide
		const size = cell.preSplit();

		//Look for a cell that can accomodate half of this cell's data
		let selected = null;
		for (const [id, obj] of this.cells) {
			if (obj.dataSize + (size / 2) < MAX_SIZE) {
				selected = id;
				break;
			}
		}

		if (selected) {
			//We found a new cell to send data to
		}

		const c = new Cell()
	}

	static write() {

	}
}