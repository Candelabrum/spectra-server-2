"use strict";

const moment = require("moment");

module.exports = class {
	constructor() {
		this.MAX_FRIENDS = 100;
		this.MAX_PENDING = 5;
		this.pending = {};
	}

	init() {
		Spectra.loadJSON('spectra-data/pendingFriends.json', (err, data) => {
			if (err) {
				crash("Pending Friends Load Error", err);
			} else {
				this.pending = data;

				Spectra.UserClass.initFunctions.push(function (callback) {
					//Setup Friends Notify Listener
					const tasks = [];
					tasks.push(new Promise((resolve, reject) => {
						database.all("SELECT friendNotify FROM users WHERE userid=$userid", {
							$userid: this.userid
						}, (err, results) => {
							if (err) {
								crash("friendNotify Load Error", err);
								resolve();
							} else if (results && results[0] && results[0].friendNotifs) {
								this.friendNotifs = (results[0].friendNotifs === 1 ? true : false);
								resolve();
							} else {
								this.friendNotifs = true;
								database.run("UPDATE users SET friendNotify = 1 WHERE userid=$userid", {
									$userid: this.userid
								}, (err) => {
									if (err) crash("Set friendnotify Err", err);
									resolve();
								});
							}
						});
					}));

					tasks.push(new Promise((resolve, reject) => {
						database.all("SELECT friend FROM friends WHERE userid=$userid", {
							$userid: this.userid
						}, (err, friends) => {
							if (err) {
								crash("Friendslist load Error", err);
							} else if (friends && friends[0]) {
								let s = new Set();
								for (const x of friends) {
									if (!x.friend || toId(x.friend) === "" || s.has(x.friend)) continue;
									s.add(x.friend);
								}
								this.friends = s;
							}
							resolve();
						});
					}));

					tasks.push(new Promise((resolve, reject) => {
						if (Friends.pending[this.userid]) {
							if (Object.keys(Friends.pending[this.userid]).length === 0) return resolve();
							let arr = Object.keys(Friends.pending[this.userid]);
							let from = "";
							let msg = arr.length > 1 ? "You have pending friend requests from" : "You have a pending friend request from";

							for (let i = 0; i < arr.length; i++) {
								from += arr[i];
								if (i < arr.length - 2) {
									from += ", ";
								} else if (i < arr.length - 1) {
									from += ", and ";
								}
							}
							this.send(`|pm| Friend Notifications|${this.getIdentity()}|/raw ${msg} ${from}.`);
						}

						resolve();
					}));

					Promise.all(tasks).then(() => {
						if (this.friends.size === 0) return callback();
						for (const friend in this.friends) {
							if (Users(toId(friend)) && Users(toId(friend)).connected) {
								Users(toId(friend)).friendNotify(this.name, "Logged In");
							}
						}
						return callback();
					})
				});

				Spectra.UserClass.prototype.friendNotify = function(name, type) {
					if (this.friendNotifs) this.send(`|pm| Friend Notifications|${this.getIdentity()}|/raw ${name} has ${type}.`);
				};

				Spectra.UserClass.prototype.getFriends = function(callback) {
					if (!this.friends || !this.friends.size === 0) return null;
					const tasks = [];
					for (const name of this.friends) {
						tasks.push(new Promise((resolve, reject) => {
							if (Users(toId(name)) && Users(toId(name)).connected) {
								resolve({
									name: name,
									seen: "Online"
								});
							} else {
								LoginServer.seen(toId(name), seen => {
									if (seen && !isNaN(seen)) {
										resolve({
											name: name,
											seen: moment(seen).fromNow()
										});
									} else {
										resolve({
											name: name,
											seen: "Never"
										});
									}
								});
							}
						}));
					}

					Promise.all(tasks).then((objects) => {
						return callback(objects);
					});
				};

				Spectra.UserClass.getFriends = function(callback, userid) {
					let set = new Set();
					database.all("SELECT friend FROM friends WHERE userid=$userid", {
						$userid: userid
					}, (err, friends) => {
						if (err) {
							crash("static getFriends load Error", err);
							return callback(null);
						} else if (friends && friends[0]) {
							for (const x of friends) {
								set.add(x.friend);
							}

							const tasks = [];
							for (const name of set) {
								tasks.push(new Promise((resolve, reject) => {
									if (Users(toId(name)) && Users(toId(name)).connected) {
										resolve({
											name: name,
											seen: "Online"
										});
									} else {
										LoginServer.seen(toId(name), seen => {
											if (seen) {
												resolve({
													name: name,
													seen: moment(seen).fromNow()
												});
											} else {
												resolve({
													name: name,
													seen: "Never"
												});
											}
										});
									}
								}));
							}

							Promise.all(tasks).then((objects) => {
								return callback(objects);
							});
						} else {
							return callback(null);
						}
					});
				}
			}
		});
		return this;
	}

	add(name1, name2) {
		console.log("Friends Add called");
		database.run("INSERT INTO friends(userid, friend) VALUES($userid, $friend)", {
			$userid: toId(name1),
			$friend: name2
		}, (err) => {
			if (err) crash("Friends add err", err);
		});
		database.run("INSERT INTO friends(userid, friend) VALUES($userid, $friend)", {
			$userid: toId(name2),
			$friend: name1
		}, (err) => {
			if (err) crash("Friends add err", err);
		});
	}

	delete(name1, name2) {
		database.run("DELETE FROM friends WHERE userid=$userid AND friend=$friend", {
			$userid: toId(name1),
			$friend: name2
		}, (err) => {
			if (err) crash("Friends delete err", err);
		});
		database.run("DELETE FROM friends WHERE userid=$userid AND friend=$friend", {
			$userid: toId(name2),
			$friend: name1
		}, (err) => {
			if (err) crash("Friends delete err", err);
		});
	}
}